### Prezentare

[Prezentare](https://docs.google.com/presentation/d/1VLJx-acbb25aQ-yZ_JFSSPvlOaNB6XMPpzxNd1uN_h0/edit?usp=sharing)

### Util

* [GitLab Unfiltered youtube channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A)
* [Learning git onboarding](https://www.youtube.com/watch?v=6VI_SqkcQIQ)
* [Wider community contribution](https://about.gitlab.com/blog/2020/06/23/3000-contributors-post/)
* [The Remote Playbook](https://learn.gitlab.com/suddenlyremote)

* [How to live your best remote life](https://about.gitlab.com/blog/2019/07/09/tips-for-working-from-home-remote-work/)
* [GitLab's guide for starting a remote job](https://about.gitlab.com/company/culture/all-remote/getting-started/)
* [Pitfalls to watch out for when embracing remote](https://about.gitlab.com/company/culture/all-remote/what-not-to-do/)
* [Mastering the all-remote environment: My top 5 challenges and solutions](https://about.gitlab.com/blog/2019/12/30/mastering-the-all-remote-environment/)
* [5 Tips for mastering video calls](https://about.gitlab.com/blog/2019/08/05/tips-for-mastering-video-calls/)
* [Mastering the at-home office environment](https://about.gitlab.com/blog/2019/09/12/not-everyone-has-a-home-office/)
* [How to make your home a space that works with kids](https://about.gitlab.com/blog/2019/08/01/working-remotely-with-children-at-home/)
* [5 Things to keep in mind while working remotely with kids](https://about.gitlab.com/blog/2019/08/08/remote-kids-part-four/)
* [Combating burnout, isolation, and anxiety](https://about.gitlab.com/company/culture/all-remote/mental-health/)
* [Adopting a self-service mindset](https://about.gitlab.com/company/culture/all-remote/self-service/)

### Intrebari si raspunsuri

1. Cum te-ai decis să alegi un job remote? A fost o alegere deliberată?

Mi-am dorit sa am libertate si flexibilitate. Am citit despre remote work si imi imaginam cum poate sa fie, am vazut la colegii mei din strainatate, si mi-am dorit sa fiu ca ei. Am inceput prin a cere managerului meu o zi pe saptamana sa lucrez de acasa(a fost o ciudatenie sa cer asta atunci) Apoi am avut sansa sa lucrez pe un proiect, full remote.

2. Ai lucrat și non-remote înainte de asta? Dacă da, cum a fost trecerea?

Am lucrat si non-remote intr-o companie de outsourcing din Tg-Mures, am lucrat 5 ani si jumatate.

3. De cât timp lucrezi remote? Îți imaginezi că vei lucra mereu așa sau vei resimți lipsa unei echipe off-line?

Lucrez remote de 4 ani si jumatate, imi place ceea ce fac, imi plac oamenii cu care lucrez, am avut sansa de a fi in echipe cu oameni faini de la care am invatat si invat enorm. Nu imi imaginez ca voi lucra intr-un birou.
E interesant ca nu simt lipsa unei echipe offline, suntem destul de conectati cu colegii de la job, atat cand vine vorba de munca dar uneori si in afara. Un alt lucru pe care doresc sa il mentionez e ca am pastrat si legatura cu o parte din fostii colegi, i-am vizitat si ne-am intalnit de cate ori am avut sansa.

Am experimentat cu coworking space, obisnuiam inainte de pandemie sa merg la un birou al unui prieten uneori. Acum nu pot sa zic ca imi lipseste mai alez ca din martie si sotul meu lucreaza de acasa.

4. Ce îți place cel mai mult la remote working?

   * [Non-linear time](https://about.gitlab.com/company/culture/all-remote/non-linear-workday/)

   * Flexibilitatea, pot sa merge sa petrec timp cu familia, merg saptamanal la sora mea, e obiceiul nostru vineri dimineata sa stam la povesti si sa ne jucam cu fetele ei.
 Am posibilitatea sa o ajut cand are nevoie.

   * Merg la sala la ora 11, cand nu e nimeni

   * Oamenii pe care am sansa sa ii cunosc si de la care sa invat, acesta e un lucru la care nu m-am gandit inainte.

   * Pot sa calatoresc  si sa lucrez, sunt la mare acum

5. Cum arată echipa cu care lucrezi? (câte persoane, din ce țări / pe ce fusuri orare)

Suntem o echipa mica, acum suntem 5 colegi, 4 programatori si Engineer Manager care este si Product Manager. Romania, Turcia, Polonia, Canada. Colaboram cu departamentul de Growth echipa mare e formata 34 de colegi din toata lumea. Islanda, Australia, India, Taiwan, Peru Columbia....

6. Faceți pair programming sessions? Cum merg ele?

Pair programming nu prea am facut, nu asa cum faceam pentru proiectul precedent. Aici se doreste sa lucram cat se poate de asincron. Nu sunt asteptari sa facem meetinguri si sa raspundem instant. Pair pragramming seamana cu un brainstorming mai degraba. Colaboram si discutam intr-un issue sau MR, venim cu una sau mai multe propuneri in care discutam, iar cand ne intalnim incercam sa luam decizii pe baza a ceea ce avem.

7. Care sunt procedurile special gândite de GitLab pentru a funcționa mai bine remote? Mă refer aici nu doar la instrumente cloud, ci și la proceduri care să favorizeze ca echipa să lucreze mai bine împreună (întâlniri on-line pentru coffee, gaming, etc.)

   * Totul e scris si documentat. Fie in handbook, in google docs, isssues sau MRs.
 Pana si team day e organizat folosind un issue https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/175
 https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/119

   * Social call programat in diferite timezones ca sa poata participa toata lumea

   * AMA sessions, differiti colegi(Sid) sau EVP Executive Vice President of Engineering, sau departamanete pe echipe, database, memory, au regulat AMA sessions sau office hours. Oricine poate sa participe si sa intrebe diferite lucruri de care sunt interesati.

   * Intalnirile au o agenda predefinita unde sunt toate intrebarile uneori intainte de call sunt deja si raspunsuri.

   * Majoritatea intalnirilor sunt inregistrate si disponibile asincron.

   * O mare parte din ele sunt publice pe canalul de youtube GitLab unfiltered

   * La GitLab avem si [grupuri pe slack ](https://about.gitlab.com/company/culture/all-remote/informal-communication/)pe diferite teme, #daily_gratitude, #dogs, #cooking, #travel, #thanks

8. Vă întâlniți și off-line? Au o frecvență aceste întâlniri?

   * Se obisnuia sa se intalneasca echipa mare la evenimente ca GitLab contribute o data la 9 luni, se intalneau toti membrii, dar cu pandemia, anul acesta au fost anulate si au fost organizate virtual.

   * Se mai intalneau local in meetupuri sau se vizitau, acum nu se incurajeaza asta datorita situatiei globale.

   * Si departamentele se intalneau periodic

9. Aveți un program ”fix” pentru fusul orar în care sunteți? Sau e totul foarte flexibil, la ”mica înțelegere” cu cei cu care trebuie să vorbești?

Programul e flexibil, nu suntem urmariti sa fim online. Eu ma asigura sa fiu prezenta, la intalnirea saptamanala cu echipa, 1:1 cu managerul meu si restul depinde de disponibilitate. Majoritatea meetingurilor sunt dupa ora 4-5, de multe ori sunt in afara orelor mere de munca. Multe le urmaresc asincron sau citesc agenda. Mindsetul e orientat catre rezultate.

10. Ce metodologie (de proces) folosiți?

https://about.gitlab.com/handbook/engineering/development/growth/acquisition-conversion-be-telemetry/#project-management-process

E numit un proces hibrid Kanban, avem flow continuu de dezvoltare, dar avem si release-uri lunare.


11. Cât de des aveți sync meetings, sau alte feluri de meetings? Mă gândesc aici la gestionarea dependințelor dintre echipe sau dintre membrii aceleiași echipe. Sunt curioasă ce efect are asta asupra modului de creare al task-urilor și asupra planificării.

Legat de planificare pot sa impartasesc ca la inceput am avut o perioada de incertitudine. Am fost obisnuita sa avem planing sessions unde planificam munca pentru sprintul urmator, discutam prioritatile cu un prodcut owner. Aici eu am fost primul programator in echipa, echipa e "tanara" si toti memberi au fost recent angajati. Experimentam si cautam ceea ce se potriveste acestei echipe. Fac parte din echipa de Product Analytics, departamentul de Growth responsabil cu cresterea si dezvoltarea produsului, construim tooluri care ne ajuta sa luam decizii informate bazate pe date legat de directia produsului.  Product Analytics e despre corelctarea informatiilor si analyzarea lor, user privacy. Acum o data pe luna discutam prioritatile si lucram in directia lor, desigur apar si lucruri neplanificate, ne adaptam. Totul e in miscare la GitLab.

Colaborarea se intampla intre toate echipele, nu doar in echipa din care fac parte. Codul este verificat de minim 2 persoane intainte sa ajunga in production, verificarea, code review poata sa faca oricine.

12. Cum se desfășoară procesul de onboarding pentru noile persoane din echipă?

Onboarding e facilitate de un isssue cu 216 subtaskuri si de un onboarding buddy care te ajuta cu ceea ce se intampla la GitLab

E organizat pe zile, cuprinde creeare de conturi pe diferiti tooluri folosite, sessiuni si video-uri pe care le urmarim, cursuri de securitate si diversity, inclusion and belonging. Setarea proiectului, primul MR, coffee chaturi cu membrii echipei

13. Ai niște tips & tricks personale?

   * Fa-ti o rutina care iti place, pentru mine asta e super important, eu ma gandesc ca e un drum lung si nu e ceva temporar.
   * creaza-ti un mediu care iti place, de la biroul in care lucrezi pana la paharul din care bei apa si oamenii cu care alegi sa iti petreci timpul.
   * Fii intr-un mindset de invatare, oricand
   * Fa si altceva pe langa job, un hobby diferit.

14. Cum masori productivitatea?

Avem indicatori de performanta pe echipa, acesta este acum 13 MRs pe luna pe membru.

Cred ca pentru mine functioneaza acum sa stiu ca ceea ce fac aduce valoare, aduce valoare unui coleg sau unui utilizator.

Pentru mine e important sa stiu ca nu blochez colegii si ca am raspuns intrebarilor care imi sunt adresate, apoi lucrez la ceea ce este in numele meu in ordinea prioritatilor.

Uneori simt ca nu lucrez suficient, ca nu am terminat.

15. Work life balance.

Inca invat asta, la GitLab ai impresia ca lumea nu doarme, in orice moment se intampla ceva, apare ceva de citit, uneori ma desprind greu de calculator.

Separarea cu un spatiu dedicat ajuta, rutina pe care o am.

